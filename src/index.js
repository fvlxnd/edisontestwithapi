initializeStartPage();

/// Переключение на страницу списка источников
document.getElementById('showSourceList').onclick = function() {
    try {
        initializeSourceList();
        document.getElementById('sourceContainer').innerHTML = `<div class="loading-screen">
            <span>Загрузка, пожалуйста подождите</span>
        </div>`;
        getSourceListNoFilter()
            .then(
                response => buildSourceList(response),
                error => console.error(error)
            );
        Array.from(document.querySelectorAll('#sideMenu ul li')).forEach(item => {
            item.classList.remove('active-menu-element');
        });
        this.classList.add('active-menu-element');
    }   catch (e) {
        console.error('Ошибка: ' + e);
    }
};

/// Переключение на страницу проверки адреса
document.getElementById('showEmailChecking').onclick = function() {
    try {
        initializeStartPage();
        Array.from(document.querySelectorAll('#sideMenu ul li')).forEach(item => {
            item.classList.remove('active-menu-element');
        });
        this.classList.add('active-menu-element');
    }  catch (e) {
        console.error('Ошибка: ' + e);
    }
};

/// Инициализация страницы проверки адресов
function initializeStartPage() {
    getSourceListNoFilter()
        .then(
            resolve => buildStartPage(resolve),
            error => console.error(error)
        );
}

function buildStartPage(option) {
    let optionContent = '';

    option = JSON.parse(option);

    option.forEach(item => {
        if(item.Domain !== '' && item.Domain !== undefined) {
            optionContent += `<option value="${item.Domain}">${item.Title}</option>`;
        }
    });

    let container = document.getElementById('sourceContainer'),
        content = `<div class="check-user-container">
            <span>Проверьте, не попал ли ваш адрес электронной почты в руки хакеров</span>
            <div class="input-container"><div class="flexbox">
                <input id="emailData" placeholder="Введите Email, например text@example.com">
                <button id="startLookForEmail" class="search-button"></button></div>
            </div>
            <div class="input-container">
                <input type="checkbox" id="selectOneSource"><label for="selectOneSource">Искать в конкретном источнике</label>
                <select id="oneSourceSelect">${optionContent}</select>
            </div>
        </div>`;

    document.getElementById('scrollTop').style.display = 'none';
    Array.from(document.getElementsByClassName('filter-container')).forEach(item => {
        item.classList.add('hidden');
    });
    container.innerHTML = content;
    container.style.width = '75%';

    handleEventsOnCheckingEmail();
}

/// Инициализация страницы со списком источников
function initializeSourceList() {
    let container = document.getElementById('sourceContainer');

    document.getElementById('scrollTop').style.display = 'block';
    Array.from(document.getElementsByClassName('filter-container')).forEach(item => {
        item.classList.remove('hidden');
    });
    container.style.width = '70%';
    container.innerHTML = '';
    document.getElementById('nameFilterData').value = '';
    document.getElementById('domainFilterData').value = '';
}

/// Promise с запросом источников без фильтров
function getSourceListNoFilter() {
    return new Promise(function(resolve, reject) {
        let xhr = new XMLHttpRequest(),
            url = `https://haveibeenpwned.com/api/v2/breaches`;

        xhr.open('GET', url, true);

        xhr.onload = function() {
            if (this.status === 200) {
                resolve(this.response);
            } else {
                let error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function() {
            reject(new Error('Ошибка соединения'));
        };

        xhr.send();
    });
}

/// Promise с запросом источников с фильтром по домену
function getSourceListDomainFilter(query) {
    return new Promise(function(resolve, reject) {
        let xhr = new XMLHttpRequest(),
            url = `https://haveibeenpwned.com/api/v2/breaches?domain=${query}`;

        xhr.open('GET', url, true);

        xhr.onload = function() {
            if (this.status === 200) {
                resolve(this.response);
            } else {
                let error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function() {
            reject(new Error('Ошибка соединения'));
        };

        xhr.send();
    });
}

/// Promise с запросом источников с фильтром по имени
function getSourceListNameFilter(query) {
    return new Promise(function(resolve, reject) {
        let xhr = new XMLHttpRequest(),
            url = `https://haveibeenpwned.com/api/v2/breach/${query}`;

        xhr.open('GET', url, true);

        xhr.onload = function() {
            if (this.status === 200) {
                resolve(this.response);
            } else {
                let error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function() {
            reject(new Error('Ошибка соединения'));
        };

        xhr.send();
    });
}

/// Promise с запросом проверки адреса
function checkEmail(mail) {
    return new Promise(function(resolve, reject) {
        let xhr = new XMLHttpRequest(),
            url = `https://haveibeenpwned.com/api/v2/breachedaccount/${mail}`;

        xhr.open('GET', url, true);

        xhr.onload = function() {
            if (this.status === 200) {
                resolve(this.response);
            } else {
                let error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function() {
            reject(new Error('Ошибка подключения к API'));
        };

        xhr.send();
    });
}

/// Построение списка источников
function buildSourceList(unparsedData) {
    let data = JSON.parse(unparsedData),
        container = document.getElementById('sourceContainer'),
        content = '';

    container.innerHTML = '';

    // Если источник всего один и пришел объект, а не массив
    if (!Array.isArray(data)) {
        data = [data];
    }

    if (data.length === 0) {
        showEmptySourceListSearchResult();
    } else {
        for (let i = 0; i < data.length; i++) {
            let compromisedData = data[i].DataClasses.join(',');
            content += `<article class="source-card">
            <div class="card-logo-side">
                <img src="${data[i].LogoPath}">
            </div>
            <div class="card-text-side">
                <span class="card-title">${data[i].Title}</span>
                <span>${data[i].Description}</span>
                <div class="compromised-data-list">
                    <b>Compromised Data: </b>
                    <span>${compromisedData}</span>
                </div>
            </div>
        </article>`;
        }

        container.innerHTML = content;

        handleEventsOnSourceList(data);
    }
}

function buildEmailList(data) {
    data = JSON.parse(data);
    let originalContent = document.getElementById('sourceContainer').innerHTML.split('<article class="source-card">')[0],
        newElements = '';

    if (data.length === 0) {
        handleCheckEmailError('Поздравляем, ваш адрес электронной почты в безопасности');
    } else {
        for (let i = 0; i < data.length; i++) {
            newElements += `<article class="source-card">
            <div class="card-logo-side">
                <img src="${data[i].LogoPath}">
            </div>
            <div class="card-text-side">
                <span class="card-title">${data[i].Title}</span>
                <span>${data[i].Description}</span>
                <div class="compromised-data-list">
                    <b>Compromised Data: </b>
                    <span>${compromisedData}</span>
                </div>
            </div>
        </article>`;
        }

        document.getElementById('sourceContainer').innerHTML = originalContent + newElements;
    }

    handleEventsOnCheckingEmail();
}

/// Обработка событий на странице со списком источников
function handleEventsOnSourceList(data) {
    // Клик по кнопке, прокручивающей список к началу
    document.getElementById('scrollTop').onclick = function() {
        try {
            document.getElementById('sourceContainer').scrollTop = 0;
        }  catch (e) {
            console.error('Ошибка: ' + e);
        }
    };
    /// Нажатие Enter для фильтрации по домену
    document.getElementById('domainFilterData').addEventListener('keydown', function(e) {
        if (e.code === 'Enter') {
            let query = document.getElementById('domainFilterData').value;
            getSourceListDomainFilter(query)
                .then(
                    response => buildSourceList(response),
                    error => showEmptySourceListSearchResult(error)
                );
            document.getElementById('nameFilterData').value = '';
        }
    });
    document.getElementById('nameFilterData').addEventListener('keydown', function(e) {
        if (e.code === 'Enter') {
            let query = document.getElementById('nameFilterData').value,
                finalQuery = searchStringConversion(query);
            if (finalQuery.length > 1) {
                getSourceListNameFilter(finalQuery)
                    .then(
                        response => buildSourceList(response),
                        error => showEmptySourceListSearchResult()
                    );
            } else {
                getSourceListNoFilter()
                    .then(
                        response => buildSourceList(response),
                        error => console.error(error)
                    );
            }
            document.getElementById('domainFilterData').value = '';
        }
    });
    // Клик по кнопке, запускающей фильтр по домену
    document.getElementById('domainFilterButton').onclick = function() {
        let query = document.getElementById('domainFilterData').value;
        getSourceListDomainFilter(query)
            .then(
                response => buildSourceList(response),
                error => showEmptySourceListSearchResult(error)
            );
        document.getElementById('nameFilterData').value = '';
    };
    // Клик по кнопке, запускающей фильтр по имени
    document.getElementById('nameFilterButton').onclick = function() {
        let query = document.getElementById('nameFilterData').value,
            finalQuery = searchStringConversion(query);
        if (finalQuery.length > 1) {
            getSourceListNameFilter(finalQuery)
                .then(
                    response => buildSourceList(response),
                    error => showEmptySourceListSearchResult(error)
                );
        } else {
            getSourceListNoFilter()
                .then(
                    response => buildSourceList(response),
                    error => console.error(error)
                );
        }
        document.getElementById('domainFilterData').value = '';
    };
}

/// Обработка событий на странице проверки адреса
function handleEventsOnCheckingEmail() {
    // Клик по кнопке поиска
    document.getElementById('startLookForEmail').onclick = function() {
        let oneSource = checkOneSource(),
            query = document.getElementById('emailData').value + oneSource;
        checkEmail(query)
            .then(
                response => buildEmailList(response),
                error => handleCheckEmailError(error)
            );
    };
    document.getElementById('emailData').addEventListener('keydown', function(e) {
        if (e.code === 'Enter') {
            let oneSource = checkOneSource(),
                query = document.getElementById('emailData').value + oneSource;
            checkEmail(query)
                .then(
                    response => buildEmailList(response),
                    error => handleCheckEmailError(error)
                );
        }
    });
}

/// Проверка того, выбран ли поиск адреса по конкретному источнику
function checkOneSource() {
    if (document.getElementById('selectOneSource').checked === true) {
        return `?domain=${document.getElementById('oneSourceSelect').value}`;
    } else {
        return '';
    }
}

/// Отображение сообщения о безуспешном поиске по источникам
function showEmptySourceListSearchResult() {
    let container = document.getElementById('sourceContainer');
    container.innerHTML = '<div class="empty-search-result-for-source-list"><span>По вашему запросу ничего не найдено</span></div>';
}

/// Отображение сообщения об ошибке подключения в проверке адреса
function handleCheckEmailError(error) {
    let originalContent = document.getElementById('sourceContainer').innerHTML.split('<article class="source-card">')[0],
        newElement = `<article class="source-card">
            <span>${error}</span>
        </article>`;

    document.getElementById('sourceContainer').innerHTML = originalContent + newElement;

    handleEventsOnCheckingEmail();
}

/// Конвертация русского языка в запросе в английский и склеивание в одно слово
function searchStringConversion(string) {
    let query = string.split(' ').join('');
    query = query.toLowerCase();
    let cyr2latChars = new Array(
        ['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'],
        ['д', 'd'],  ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'],
        ['и', 'i'], ['й', 'i'], ['к', 'k'], ['л', 'l'],
        ['м', 'm'],  ['н', 'n'], ['о', 'o'], ['п', 'p'],  ['р', 'r'],
        ['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'],
        ['х', 'h'],  ['ц', 'c'], ['ч', 'ch'],['ш', 'sh'], ['щ', 'shch'],
        ['ъ', ''],  ['ы', 'y'], ['ь', ''],  ['э', 'e'], ['ю', 'yu'], ['я', 'ya'],

        ['А', 'A'], ['Б', 'B'],  ['В', 'V'], ['Г', 'G'],
        ['Д', 'D'], ['Е', 'E'], ['Ё', 'YO'],  ['Ж', 'ZH'], ['З', 'Z'],
        ['И', 'I'], ['Й', 'I'],  ['К', 'K'], ['Л', 'L'],
        ['М', 'M'], ['Н', 'N'], ['О', 'O'],  ['П', 'P'],  ['Р', 'R'],
        ['С', 'S'], ['Т', 'T'],  ['У', 'U'], ['Ф', 'F'],
        ['Х', 'H'], ['Ц', 'C'], ['Ч', 'CH'], ['Ш', 'SH'], ['Щ', 'SHCH'],
        ['Ъ', ''],  ['Ы', 'Y'],
        ['Ь', ''],
        ['Э', 'E'],
        ['Ю', 'YU'],
        ['Я', 'YA'],

        ['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'],
        ['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'],
        ['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'],
        ['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'],
        ['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'],
        ['z', 'z'],

        ['A', 'A'], ['B', 'B'], ['C', 'C'], ['D', 'D'],['E', 'E'],
        ['F', 'F'],['G', 'G'],['H', 'H'],['I', 'I'],['J', 'J'],['K', 'K'],
        ['L', 'L'], ['M', 'M'], ['N', 'N'], ['O', 'O'],['P', 'P'],
        ['Q', 'Q'],['R', 'R'],['S', 'S'],['T', 'T'],['U', 'U'],['V', 'V'],
        ['W', 'W'], ['X', 'X'], ['Y', 'Y'], ['Z', 'Z'],

        [' ', '_'],['0', '0'],['1', '1'],['2', '2'],['3', '3'],
        ['4', '4'],['5', '5'],['6', '6'],['7', '7'],['8', '8'],['9', '9'],
        ['-', '-']

    );
    let newStr = new String();
    for (let i = 0; i < query.length; i++) {
        let ch = query.charAt(i),
            newCh = '';
        for (let j = 0; j < cyr2latChars.length; j++) {
            if (ch == cyr2latChars[j][0]) {
                newCh = cyr2latChars[j][1];
            }
        }
        newStr += newCh;
    }
    return newStr.replace(/[_]{2,}/gim, '_').replace(/\n/gim, '');
}
